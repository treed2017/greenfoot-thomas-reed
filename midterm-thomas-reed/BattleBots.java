

import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;
import greenfoot.World;
import java.util.List;

public class BattleBots extends World {
    private static final int WIDTH = 900;
    private static final int HEIGHT = 600;
    private static final int CELL_SIZE = 1;
    private static final int BUFFER = 100;
    private static String playAgain = "";
    private Robot frogBot;
    private Robot tuxBot;
    private Robot SnowBot;
    private Robot winner;
    private Robot loser;
    private ResultMessage resultMessage;
    private GreenfootImage frogWins;
    private GreenfootImage tuxWins;
    private GreenfootImage blank;
    private boolean running = true;
    private boolean gameOver = false;
    

    public BattleBots() {
        super(900, 600, 1);
        this.placeRobots();
        this.resultMessage = new ResultMessage();
        int x = this.getWidth() / 2;
        int y = this.getHeight() / 2;
        this.addObject(this.resultMessage, x, y);
        this.blank = new GreenfootImage("blankMessage.png");
        this.frogWins = new GreenfootImage("frogBotWin.png");
        this.tuxWins = new GreenfootImage("tuxBotWin.png");
        prepare();
        Scoreboard scoreboard = new Scoreboard();
        addObject(scoreboard,900,0);
        scoreboard.setLocation(550,375);
        scoreboard.setLocation(538,375);
        scoreboard.setLocation(542,375);
       
    }

    public void placeRobots() {
        this.frogBot = new FrogBot();
        this.tuxBot = new TuxBot();
        this.SnowBot = new BallBot();
        int x = Greenfoot.getRandomNumber(250) + 100;
        int y = Greenfoot.getRandomNumber(400) + 100;
        this.addObject(this.frogBot, x, x);
        x = Greenfoot.getRandomNumber(250) + 450 + 100;
        y = Greenfoot.getRandomNumber(400) + 100;
        this.addObject(this.tuxBot, x, y);
        //this.addObject(this.SnowBot, y, y);
    }

    public void act() {
        if(this.running && this.frogBot.foundOtherBot()) {
            Greenfoot.stop();
            this.running = false;
        }

    }

    public void stopped() {
        for(int winnerImage = 0; winnerImage < 10; ++winnerImage) {
            this.frogBot.fight();
            this.tuxBot.fight();
            this.SnowBot.fight();
             
        }

        if(Greenfoot.getRandomNumber(100) < 50) {
            this.winner = this.frogBot;
            this.loser = this.tuxBot;
           
            this.resultMessage.setImage(this.frogWins);
            Scoreboard.frog++;
        } else {
            this.winner = this.tuxBot;
            this.loser = this.frogBot;
            
            this.resultMessage.setImage(this.tuxWins);
            Scoreboard.tux++;
        }

       this.removeObject(this.loser);
        this.winner.setRotation(0);
        this.winner.setLocation(75, 75);
        GreenfootImage var2 = this.winner.getImage();
        var2.scale(var2.getWidth() * 2, var2.getHeight() * 2);
        if(!this.gameOver) {
            playAgain = Greenfoot.ask("Would you like to play again? Y/N");
            if(!playAgain.equals("y") && !playAgain.equals("Y")) {
                this.gameOver = true;
            } else {
                List remove = getObjects( Robot.class );
               /* for (Object objects : remove){
                    try{
                        removeObject( ( Robot ) objects );
                    }
                    catch(Exception e){
                       System.out.println(e.getMessage());
                    }
                }
                */
                prepare();
                this.running = true;
                Greenfoot.start();
            }
        }

        this.resultMessage.setImage(this.blank);
    }

    public boolean isRunning() {
        return this.running;
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        BallBot ballbot = new BallBot();
        addObject(ballbot,584,446);
        ballbot.setLocation(555,330);
        removeObject(ballbot);
        BallBot ballbot2 = new BallBot();
        addObject(ballbot2,498,354);
        ballbot2.setLocation(704,446);
        removeObject(ballbot2);
    }
}
