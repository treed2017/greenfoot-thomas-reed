
import greenfoot.Actor;
import greenfoot.Greenfoot;
import java.util.List;

public abstract class Robot extends Actor {
    protected static int count = 0;
    protected static final int WAIT_COUNT = 10;
    private List<Robot> enemyList;
    private BattleBots world;

    public Robot() {
    }

    public void act() {
        this.world = (BattleBots)this.getWorld();
        if(this.world.isRunning()) {
            if(count < 10) {
                ++count;
            } else {
                this.move();
                count = 0;
            }
        }

    }

    public void move() {
        this.enemyList = this.getNeighbours(this.getMaxDistance(), true, Robot.class);
        Robot target = null;
        if(this.enemyList.size() == 1) {
            target = (Robot)this.enemyList.get(0);
        } else if(this.enemyList.size() > 1) {
            target = (Robot)this.enemyList.get(0);
        }

        if(target != null) {
            if(Greenfoot.getRandomNumber(100) < 30) {
                this.turnTowards(target.getX(), target.getY());
            } else {
                this.turn(Greenfoot.getRandomNumber(181) - 90);
            }

            this.move(Greenfoot.getRandomNumber(11) + 5);
        }

    }

    public int getMaxDistance() {
        double xSquared = Math.pow((double)this.getWorld().getWidth(), 2.0D);
        double ySquared = Math.pow((double)this.getWorld().getHeight(), 2.0D);
        return (int)Math.sqrt(xSquared + ySquared);
    }

    public boolean foundOtherBot() {
        return this.isTouching(Robot.class);
    }

    public void fight() {
        this.move(100);
        this.turn(180);
        Greenfoot.delay(5);
    }
}
